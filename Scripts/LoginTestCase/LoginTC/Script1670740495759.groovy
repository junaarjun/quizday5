import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('Pre-condition')

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.comment('Validasi')

WebUI.verifyElementVisible(findTestObject('Homepage/page_title'))

WebUI.comment('Step')

WebUI.click(findTestObject('Homepage/menu_toggle'))

WebUI.click(findTestObject('NavigasiMenu/menu_Login'))

WebUI.comment('Validasi')

WebUI.verifyElementVisible(findTestObject('FormLogin/field_username'))

WebUI.setText(findTestObject('FormLogin/field_username'), username)

WebUI.setText(findTestObject('FormLogin/field_password'), password)

WebUI.click(findTestObject('FormLogin/button_Login'))

WebUI.comment('Validasi')

if (WebUI.verifyElementPresent(findTestObject('FormLogin/message_login_failed'), 5, FailureHandling.OPTIONAL)) {
    assert true
} else if (WebUI.verifyElementPresent(findTestObject('FormAppointment/button_Book Appointment'), 5, FailureHandling.OPTIONAL)) {
    assert true
} else {
    assert false
}

WebUI.closeBrowser()


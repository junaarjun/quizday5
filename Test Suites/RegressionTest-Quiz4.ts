<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RegressionTest-Quiz4</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>3aa493ba-64ed-46c3-b260-2b1e1e46eea2</testSuiteGuid>
   <testCaseLink>
      <guid>551e4c97-efe0-4769-b051-8773bb5254ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC-001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5584c7d1-ac3b-436f-8ded-5581d442298f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC-002-004</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7e40825f-40b9-45e1-a52c-1d5eac19d723</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Credential</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>7e40825f-40b9-45e1-a52c-1d5eac19d723</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>d40fe98a-db0c-4aa6-8eb2-2bd226eed6a3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7e40825f-40b9-45e1-a52c-1d5eac19d723</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>5a48d633-c998-421b-8c60-675aa8d88fb3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>682f4a76-7882-4e63-83dc-fc7f4e78e5f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC-005</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>57bc4b28-63a0-46d6-b315-2021ed4d2ebb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BookAppointment/TC-006</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4675a40f-bd29-4172-b38b-db212d93f1c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BookAppointment/TC-007</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>

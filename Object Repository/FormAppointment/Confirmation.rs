<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Confirmation</name>
   <tag></tag>
   <elementGuidId>3652c152-09f5-45ac-860d-0c078b3e5675</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h2[contains(text(), &quot;Appointment Confirmation&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
